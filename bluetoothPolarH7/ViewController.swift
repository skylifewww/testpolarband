import UIKit
import CoreBluetooth

class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    var centralManager:CBCentralManager!
    var connectingPeripheral:CBPeripheral!
    
    @IBOutlet var label1: UILabel!
    @IBOutlet weak var heartImage: UIImageView!
    @IBOutlet weak var scannoButton: UIButton!
    @IBOutlet weak var deviceButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let POLARH7_HRM_HEART_RATE_SERVICE_UUID = "180D"
    let POLARH7_HRM_DEVICE_INFO_SERVICE_UUID = "180A"
    var isUpdate = false
    
    override func viewDidLoad() {
        
        
        
        self.heartImage.isHidden = true
        self.deviceButton.isHidden = true
        self.label1.isHidden = true
        activityIndicator.hidesWhenStopped = true
 
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func scannButtonTapped(_ sender: UIButton) {
        activityIndicator.startAnimating()
        
        let heartRateServiceUUID = CBUUID(string: POLARH7_HRM_HEART_RATE_SERVICE_UUID)
        let deviceInfoServiceUUID = CBUUID(string: POLARH7_HRM_DEVICE_INFO_SERVICE_UUID)
        
        let services = [heartRateServiceUUID, deviceInfoServiceUUID];
        let centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
        
        centralManager.scanForPeripherals(withServices: services, options: nil)
        self.centralManager = centralManager;
        scannoButton.isEnabled = false
    }
    
    @IBAction func deviceButtonTapped(_ sender: UIButton) {
        activityIndicator.removeFromSuperview()
        self.heartImage.isHidden = false
        self.label1.isHidden = false
        self.deviceButton.isHidden = true
        self.fadeOut(finished: true)
        isUpdate = true
    }
    
    //MARK:- Heart Animations Metods
    private func fadeIn(finished: Bool) {
            UIView.animate(withDuration:1.0, delay: 0, options: [.curveEaseOut], animations: { self.heartImage.transform = CGAffineTransform(scaleX: 1.3, y: 1.3) } , completion: self.fadeOut)
    }
    private func fadeOut(finished: Bool) {
            UIView.animate(withDuration:1.0, delay: 0, options: [.curveEaseOut], animations: { self.heartImage.transform = CGAffineTransform.identity } , completion: self.fadeIn)
    }
    
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("--- centralManagerDidUpdateState")
        switch central.state{
        case .poweredOn:
            print("poweredOn")
            
            let serviceUUIDs:[AnyObject] = [CBUUID(string: "180D")]
            let lastPeripherals = centralManager.retrieveConnectedPeripherals(withServices: serviceUUIDs as! [CBUUID])
            
            if lastPeripherals.count > 0{
                let device = lastPeripherals.last! as CBPeripheral;
                connectingPeripheral = device;
                centralManager.connect(connectingPeripheral, options: nil)
            }
            else {
                centralManager.scanForPeripherals(withServices: serviceUUIDs as? [CBUUID], options: nil)
                
            }
        case .poweredOff:
            print("--- central state is powered off")
        case .resetting:
            print("--- central state is resetting")
        case .unauthorized:
            print("--- central state is unauthorized")
        case .unknown:
            print("--- central state is unknown")
        case .unsupported:
            print("--- central state is unsupported")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("--- didDiscover peripheral")
        
        if let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String{
            print("--- found heart rate monitor named \(localName)")
            
            self.deviceButton.setTitle(localName, for: UIControlState.normal)
            self.deviceButton.isHidden = false
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
            self.centralManager.stopScan()
            connectingPeripheral = peripheral
            connectingPeripheral.delegate = self
            centralManager.connect(connectingPeripheral, options: nil)
        }else{
            print("!!!--- can't unwrap advertisementData[CBAdvertisementDataLocalNameKey]")
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("--- didConnectPeripheral")
        
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        print("--- peripheral state is \(peripheral.state.rawValue)")
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if (error) != nil{
            print("!!!--- error in didDiscoverServices: \(String(describing: error?.localizedDescription))")
        }
        else {
            print("--- error in didDiscoverServices")
            for service in (peripheral.services as [CBService]?)!{
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if (error) != nil{
            print("!!!--- error in didDiscoverCharacteristicsFor: \(String(describing: error?.localizedDescription))")
        }
        else {
            
            if service.uuid == CBUUID(string: "180D"){
                for characteristic in service.characteristics! as [CBCharacteristic]{
                    switch characteristic.uuid.uuidString{
                        
                    case "2A37":
                        // Set notification on heart rate measurement
                        print("Found a Heart Rate Measurement Characteristic")
                        peripheral.setNotifyValue(true, for: characteristic)
                        
                    case "2A38":
                        // Read body sensor location
                        print("Found a Body Sensor Location Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A29":
                        // Read body sensor location
                        print("Found a HRM manufacturer name Characteristic")
                        peripheral.readValue(for: characteristic)
                        
                    case "2A39":
                        // Write heart rate control point
                        print("Found a Heart Rate Control Point Characteristic")
                        
                        var rawArray:[UInt8] = [0x01];
                        let data = NSData(bytes: &rawArray, length: rawArray.count)
                        peripheral.writeValue(data as Data, for: characteristic, type: CBCharacteristicWriteType.withoutResponse)
                        
                    default:
                        print()
                    }
                    
                }
            }
        }
    }
    
    func update(heartRateData:Data){
        print("--- UPDATING ..")
        var buffer = [UInt8](repeating: 0x00, count: heartRateData.count)
        heartRateData.copyBytes(to: &buffer, count: buffer.count)
        
        var bpm:UInt16?
        if (buffer.count >= 2){
            if (buffer[0] & 0x01 == 0){
                bpm = UInt16(buffer[1]);
            }else {
                bpm = UInt16(buffer[1]) << 8
                bpm =  bpm! | UInt16(buffer[2])
            }
        }
        
        if let actualBpm = bpm{
            print(actualBpm)
            label1.text = ("\(actualBpm)")
        }else {
            label1.text = ("\(bpm!)")
            print(bpm!)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("--- didUpdateValueForCharacteristic")
        
        if (error) != nil{
            
        }else {
            switch characteristic.uuid.uuidString{
            case "2A37":
                if isUpdate {
                    update(heartRateData:characteristic.value!)
                }
                
                
            default:
                print("--- something other than 2A37 uuid characteristic")
            }
        }
    }
}
